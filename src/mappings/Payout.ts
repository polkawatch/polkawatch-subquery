import {SubstrateExtrinsic,SubstrateEvent,SubstrateBlock} from "@subql/types";

import {EraIndex, Balance, AccountId} from "@polkadot/types/interfaces";

import {Heartbeat, LastHeartbeat} from "../types";

import {getParentInfo, getEraStaking, recordPayoutEra, getPayoutEra, getNetworkGroupName} from "../utils";

import {Client} from "@elastic/elasticsearch";
const elastic = new Client({ node: 'http://elasticsearch:9200' });

import maxmind, {Reader} from "maxmind";
import {getTimestamp} from "./Time";

async function getValidatorInfo(validator){
    let ret={};
    const ipv4=await lastSeenIPV4(validator.toString());

    // get information derived form last seen IPV4

    if(ipv4) {
        logger.debug("Validator last seeing at: " + ipv4);
        const asnLookup = await asnLookupOpen();
        const countryLookup = await contryLookupOpen();
        const asn = asnLookup.get(ipv4);
        const country = countryLookup.get(ipv4);

        ret["validator_country_group_code"] = country.continent.code;
        ret["validator_country_group_name"] = country.continent.names.en;
        ret["validator_country_code"] = country.registered_country.iso_code;
        ret["validator_country_name"] = country.registered_country.names.en;

        //TODO payout.asnGroupCode and Name
        ret["validator_asn_code"] = asn.autonomous_system_number;
        ret["validator_asn_name"] = asn.autonomous_system_organization;
        ret["validator_asn_group"] = getNetworkGroupName(asn.autonomous_system_organization);

    }

    // get information derived from the parent

    let parentInfo= await getParentInfo(api,validator);
    if(parentInfo){
        ret["validator_parent"]=parentInfo.parent;
        ret["validator_parent_name"]=parentInfo.name;
        ret["validator_parent_web"]=parentInfo.web;
        ret["validator_parent_legal"]=parentInfo.legal;
    }

    return ret;
}

const dbs="/usr/local/lib/node_modules/geolite2-redist/dbs";

async function asnLookupOpen(): Promise<Reader<any>>{
    return maxmind.open(dbs+"/GeoLite2-ASN.mmdb");
}

async function contryLookupOpen(): Promise<Reader<any>>{
    return maxmind.open(dbs+"/GeoLite2-Country.mmdb");
}

async function lastSeenIPV4(validator){
    const lhb= await LastHeartbeat.get(validator);
    if(lhb){
        const hb=await Heartbeat.get(lhb.heartbeatId);
        return hb.ipv4;
    }
}


export async function hanblePayout(event: SubstrateEvent): Promise<void> {

    const blockNum=event.block.block.header.number.toNumber();
    const eventIdx=event.idx;
    const eventId =`${blockNum}-${eventIdx}`;

    const {event: {data: [eraId, validatorId]}} = event;

    const era=(eraId as EraIndex).toBigInt();
    const validator=(validatorId as AccountId).toString();

    logger.info(`PayoutStarted at ${eventId} for era ${era}`);
    logger.debug(JSON.stringify(event.event));

    recordPayoutEra(blockNum,eventIdx,era);

    const validatorInfo= await getValidatorInfo(validator);

    // index on ElasticSearch

    await elastic.index({
        index: 'pw_payout',
        id: eventId,
        body: {
            date: getTimestamp(event.block.block.header.number.toBigInt()),
            era: era.toString(),
            validator: validator,

            validator_parent: validatorInfo["validator_parent"],
            validator_parent_name: validatorInfo["validator_parent_name"],
            validator_parent_web: validatorInfo["validator_parent_web"],
            validator_parent_legal: validatorInfo["validator_parent_legal"],

            validator_country_group_code: validatorInfo["validator_country_group_code"],
            validator_country_group_name: validatorInfo["validator_country_group_name"],
            validator_country_code: validatorInfo["validator_country_code"],
            validator_country_name: validatorInfo["validator_country_name"],
            validator_asn_code: validatorInfo["validator_asn_code"],
            validator_asn_name: validatorInfo["validator_asn_name"],
            validator_asn_group: validatorInfo["validator_asn_group"]

        }
    });

}


export async function hanbleReward(event: SubstrateEvent): Promise<void> {
    const blockNum=event.block.block.header.number.toNumber();
    const eventIdx=event.idx;
    const eventId =`${blockNum}-${eventIdx}`;

    const timeStamp = getTimestamp(event.block.block.header.number.toBigInt());
    const extrinsic=event.extrinsic.extrinsic.toHuman();

    // TODO: Last seen Era is incorrect, because some blocks will contain payStakers to multiple eras
    // and payStakers are processed before Rewards events.

    const era=getPayoutEra(blockNum,eventIdx,logger);

    logger.info(`Rewarded at ${eventId} with last seen era ${era}`);
    const {event: {data: [accountId, newReward]}} = event;

    const nominator=(accountId as AccountId).toString();
    const reward=(newReward as Balance).toBigInt();

    logger.debug(JSON.stringify(event));

    // Which validator rewarded me?
    const eraStaking= await getEraStaking(api,era, logger);
    if(eraStaking[nominator]){
        const validator=eraStaking[nominator].validator;
        const nominationValue=eraStaking[nominator].value;
        const validatorInfo=await getValidatorInfo(validator);

        await elastic.index({
            index: 'pw_reward',
            id: eventId,
            body: {
                date: timeStamp,
                era: era.toString(),

                validator: validator,
                nominator: nominator,
                reward: reward.toString(),
                nomination_value: nominationValue.toString(),

                validator_parent: validatorInfo["validator_parent"],
                validator_parent_name: validatorInfo["validator_parent_name"],
                validator_parent_web: validatorInfo["validator_parent_web"],
                validator_parent_legal: validatorInfo["validator_parent_legal"],

                validator_country_group_code: validatorInfo["validator_country_group_code"],
                validator_country_group_name: validatorInfo["validator_country_group_name"],
                validator_country_code: validatorInfo["validator_country_code"],
                validator_country_name: validatorInfo["validator_country_name"],
                validator_asn_code: validatorInfo["validator_asn_code"],
                validator_asn_name: validatorInfo["validator_asn_name"],
                validator_asn_group: validatorInfo["validator_asn_group"]

            }
        });
    }
    else logger.warn(`Nominator ${nominator} is not found in stakers for era ${era}`);

    // now we get the era from the extrinsic... which sometimes is a payoutStakers and sometimes a batch
    return Promise.resolve();
}