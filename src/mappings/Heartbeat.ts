import {SubstrateExtrinsic,SubstrateEvent,SubstrateBlock} from "@subql/types";

import {Heartbeat as SubstrateHeartbeat} from "@polkadot/types/interfaces/imOnline";

import {substrateKeyOwner, getPublicIP4, purgePrivateMultiaddresses} from "../utils";

import {Heartbeat, LastHeartbeat} from "../types";

export async function handleHeartbeat(event: SubstrateEvent): Promise<void> {
    const eventId =`${event.block.block.header.number.toNumber()}-${event.idx}`;
    const heartbeat = new Heartbeat(
        eventId
    );

    logger.debug("HEARTBEAT: "+eventId);

    const shb=event.extrinsic.extrinsic.method.args[0] as SubstrateHeartbeat;

    const addresses=getPublicIP4(shb.networkState.externalAddresses.toHuman(),logger);
    if(addresses.length==0) logger.warn(`No public IP report at ${eventId}`);

    heartbeat.blockNumber=event.block.block.header.number.toBigInt();
    heartbeat.authorityId=event.event.data[0].toString();
    heartbeat.validator=await substrateKeyOwner(api,heartbeat.authorityId);
    heartbeat.peerId=shb.networkState.peerId.toHex();
    heartbeat.networkAddress=purgePrivateMultiaddresses(shb.networkState.externalAddresses.toHuman());
    heartbeat.ipv4=addresses[0];
    heartbeat.multiAddress=(addresses.length>1);

    logger.debug(JSON.stringify(event.extrinsic.extrinsic.toHuman()));

    //logger.info(JSON.stringify(event.block.block.header.timestap);

    await heartbeat.save();

    // Store the Last Received HB as Indexing Aid
    // Will create or replace the last
    // Assumes all handlers are processed in parallel as blockchain is indexes
    // requires reasonable order in block processing but IP data does not change often.

    let lhb= await LastHeartbeat.get(heartbeat.validator);
    if (!lhb) lhb=new LastHeartbeat(heartbeat.validator);
    lhb.heartbeatId=eventId;

    await lhb.save();

}
