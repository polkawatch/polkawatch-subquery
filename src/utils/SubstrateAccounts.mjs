
import  { Multiaddr } from "multiaddr";


/**
 *
 */

export class Account{
  constructor(address){
    this.address=address;
  }

  /**
   * Returns the ultimate Account that controls this account
   * @returns {*}
   */
  getCanonicalAccount(){

    // TODO: this is a recurrent search for which account controls this one.
    // We can recursively search for it
    // There could be loops in navigating the following data structure
    // This initial implementation will probably not be functional enough
    // But will be enough to start testing with some data.
    if(this.getParentAccount()) return this.getParentAccount().getCanonicalAccount();
    else if(this.getControllerAccount()) return this.getControllerAccount().getCanonicalAccount();
    else if(this.getProxyAccount()) return this.getParentAccount().getCanonicalAccount();
    else return this;
  }

  /**
   * Retruns the controller account if there is one.
   * @returns {undefined}
   */
  getControllerAccount(){
    return null;
  }

  /**
   * returns the proxy account if there is one.
   * @returns {null}
   */
  getProxyAccount(){
    return null;
  }

  /**
   * returns parent account if one was specified in the ID.
   * @returns {null}
   */
  getParentAccount(){
    return null;
  }
}


/**
 * Creates a Validator given its Polkadot Address
 */
export class Validator extends Account{

  /**
   * Returns the address of the p2p node in libp2p format
   * @returns {Multiaddr}
   */
  getMultiaddrs(){
    return new Multiaddr("/ip4/127.0.0.1/udp/1234");
  }

  /**
   * Returns an ID of the network operator of the p2p
   * @returns {string}
   */
  getASN(){
    let addr=this.getMultiaddrs();
    //TODO: some magic or DB consultation
    return "Unknown";
  }

}

export class Nomiator extends Account{

}