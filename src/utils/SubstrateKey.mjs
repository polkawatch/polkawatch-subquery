import { decodeAddress, encodeAddress } from '@polkadot/keyring';
import { hexToU8a, isHex, u8aToHex } from'@polkadot/util';

export async function substrateKeyOwner(api, key, network=0, keyType='0x696d6f6e'){
  if(!isHex(key)) key=u8aToHex(decodeAddress(key));
  const owner=await api.query.session.keyOwner([keyType,key]);
  if(owner.isEmpty || owner==="") return "UNKNOWN";
  return encodeAddress(owner.toHex(),network);
}


import LRU from "lru-cache";


/**
 * Will read an info field, detecting and decoding it in the case it is HEX
 */
export function decodeInfoField(field){
  if(isHex(field.Raw)) return new Buffer(hexToU8a(field.Raw)).toString();
  else return field.Raw;
}

const parentCache={};

/**
 * Returns basic information about a validator, its parent and its parent information
 * It is cached in memory for the session, as the information is presumed stable and in the orther of thousands only.
 * @param api
 * @param validator
 * @returns {Promise<*>}
 */
export async function getParentInfo(api, validator){
  if(parentCache[validator]) return parentCache[validator];
  else{

    let parentInfo={};

    // get the parent or use itself

    let parent = await api.query.identity.superOf(validator);
    if(parent.isNone) parent=validator;
    else parent=parent.unwrap().toHuman()[0];

    parentInfo["parent"]=parent;

    // get the information

    let info =  await api.query.identity.identityOf(parent);
    if(info.isSome){
      info = info.unwrap().toHuman().info;
      if(info.display !== "None") parentInfo["name"]=decodeInfoField(info.display);
      if(info.web !== "None") parentInfo["web"]=decodeInfoField(info.web);
      if(info.legal !== "None") parentInfo["legal"]=decodeInfoField(info.legal);
    }

    // cache this information in memory
    parentCache[validator]=parentInfo;

    return parentInfo;
  }
}


/**
 * This cache has around 20K entries, possibly more in the future, so, we don't what to deal with
 * too many of this ones, on the other hand, this requests are heavy, we don't want to destroy the RPC server
 * so, we pick a reasonable max
 * @type {LRUCache}
 */
const eraStakingCache=new LRU({
  max:100
});

// TODO: not sure if this will work when subQuery is running on the CURRENT era... perhaps should then wait until closed
export async function getEraStaking(api, era, logger=console){

  if(eraStakingCache.has(era)){
    logger.info(`Cache hit for staking info for Era ${era}`);
    return Promise.resolve(eraStakingCache.get(era));
  }
  else{
    logger.info(`Retrieving staking info for Era ${era}`);
    // find out who validated in the era
    let eraRewardPoints = await api.query.staking.erasRewardPoints(era);
    const activeEraValidators=Object.keys(eraRewardPoints.toHuman().individual);

    // for each validator get who nominated it
    let args = activeEraValidators.map((v) => [era,v]);

    const validators = await api.query.staking.erasStakers.multi(args);

    let ret={};
    validators.forEach((v,i) => {
      let vacc=args[i][1];
      const nominators=v.others;

      nominators.forEach((n,j)=>{
        let nacc=n.who;
        let nvalue=n.value.toBigInt();
        ret[nacc]={
          validator: vacc,
          value: nvalue
        };
      });
    });
    // cache result and return
    eraStakingCache.set(era,ret);
    return Promise.resolve(ret);
  }
}

const eraPayoutCache=new LRU({
  max:10
});

/**
 * We need to track when a Payout event takes place in chain because the Reward event
 * happens right after it and subquery does not call handlers in order. This allows us to disambiguate
 * when several Payouts take place in the same block.
 * @param block
 * @param eventIndex
 * @param era
 * @returns {Promise<void>}
 */
export function recordPayoutEra(block, eventIndex, era){
  let blockPayouts;
  if (eraPayoutCache.has(block)) blockPayouts=eraPayoutCache.get(block);
  else blockPayouts={};
  // record this block payout
  blockPayouts[eventIndex]=era;
  // cache the data
  eraPayoutCache.set(block,blockPayouts);
}

/**
 * From all the payouts cached we return the last one that took place before the event
 * @param block
 * @param eventIndex
 * @param logger
 */
export function getPayoutEra(block,eventIndex, logger=console){
  if(eraPayoutCache.has(block)){
    let blockPayouts=eraPayoutCache.get(block);
    let payoutAtEventsIds=Object.keys(blockPayouts).map(i=>parseInt(i));

    // filter out any payout event AFTER the event, sort, return the last one
    let eraId=payoutAtEventsIds
        .filter(eid => eid < eventIndex)
        .sort((a,b)=>a-b)
        .pop();

    return blockPayouts[eraId];
  }
  else logger.error(`Expected payout in block ${block} before event ${eventIndex} is missing`);
}