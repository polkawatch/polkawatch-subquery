import  { Multiaddr } from "multiaddr";
import is_private_ip from "private-ip";

/**
 * Will clean up an array of MultiAddresses of private or invalid entries
 *
 * @param addresses Array of MultiAddresses strings
 * @param logger Logger object, defaults to console
 */
export function purgePrivateMultiaddresses(addresses, logger=console){
    return addresses.filter(address=>{
        // DISABLED due to issue #1
        return address;
        /*
        try{
            const addr=new Multiaddr(address);
            if(addr.nodeAddress().family === 4 ){
                if(!is_private_ip(addr.nodeAddress().address)) return true;
            }
            else{
                logger.warn("Unexpected Address Family");
            }
            return false;
        }
        catch(err){
            logger.error("Error while parsing address: "+err);
            return false;
        }
        */
    });
}

// Workaournd to issue #1, note this is by no means a complete multiaddress regexp implementation
export const multiaddressRegexp=new RegExp("/ip(\\d+)/(.*)/(tcp|udp)/(\\d+)");

/**
 * Regexp based alternative
 * @param addresses
 * @param logger
 * @returns {*}
 */
export function purgePrivateMultiaddressesRE(addresses, logger=console){
    return addresses.filter(address=> {
        if (multiaddressRegexp.test(address)) {
            let a = multiaddressRegexp.exec(address);
            if (a[1] === "4") {
                if (!is_private_ip(a[2])) return true;
            } else logger.warn("Unexpected Address Family");
        } else logger.warn("Unexpected address format");
        return false;
    });
}

export function getPublicIP4RE(addresses,logger=console){
    addresses=purgePrivateMultiaddressesRE(addresses);
    addresses=addresses.map(address=>{
        let a= multiaddressRegexp.exec(address);
        return a[2];
    });
    // remove duplicates
    return addresses.filter((a,i)=> addresses.indexOf(a)===i);
}


/**
 * Will return a network group name by pattern matching the ASN Organization Name
 * TODO: Very simple lookup using a coporate keywork.
 * ASN does not group Network by Corportate owner, but we can use the name and well known operators to do it.
 * @param asnName
 */
export function getNetworkGroupName(asnName){

    // If the organization name matches any of this, will be returned as group

    let group=[
        "GOOGLE",
        "AMAZON",
        "OVH",
        "MICROSOFT",
        "DIGITALOCEAN",
        "ALIBABA",
        "HETZNER"
    ].filter( v=> asnName.toUpperCase().indexOf(v)>=0 );

    if(group.length) return group[0];
    else return asnName;
}