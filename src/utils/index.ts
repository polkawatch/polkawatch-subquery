// @ts-ignore
import {getNetworkGroupName} from "./Multiaddress";

export {substrateKeyOwner, getParentInfo, getEraStaking, getPayoutEra, recordPayoutEra} from "./SubstrateKey";

// @ts-ignore
export {purgePrivateMultiaddressesRE as purgePrivateMultiaddresses} from "./Multiaddress";

// @ts-ignore
export {getPublicIP4RE as getPublicIP4, getNetworkGroupName} from "./Multiaddress";


