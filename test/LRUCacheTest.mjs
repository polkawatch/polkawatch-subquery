import {describe, it} from "mocha";
import "it-fails";

import chai from "chai";
const {assert}=chai;

import LRU from "lru-cache";

describe("It Will test the LRU Cache", function(){
    let cache=new LRU({
        max:2
    });

    it("Will add an object in cache", function(){
        cache.set("test",1);
        assert(cache.has("test"));
        assert(!cache.has("missing"));
    });

    it("Will test cache eviction", function(){
        cache.set("test1",1);
        cache.set("test2",2);
        cache.set("test3",2);
        assert(cache.has("test2"));
        // this one has been evicted
        assert(!cache.has("test1"));
    });
});