import {describe, it} from "mocha";
import chai from "chai";
import "it-fails";

const {assert}=chai;

import { ApiPromise, WsProvider } from '@polkadot/api';
import { Keyring } from '@polkadot/api';
import { decodeAddress, encodeAddress } from '@polkadot/keyring';
import { hexToU8a, isHex, u8aToHex } from'@polkadot/util';

import {substrateKeyOwner, decodeInfoField, getParentInfo} from "../src/utils/SubstrateKey.mjs";

// Allow for endpoints to be configured later on
const endpoint= process.env.PW_POLKADOT_RPC || 'wss://rpc.polkadot.io';

describe("It will test Substrate Address API", function(){
  const PHRASE="upon run bring street chef behind what question remove leaf embark gold";
  let api;
  let keyring,newPair;

  before("Will Create the API ", async function(){
    this.timeout(10000);
    const wsProvider = new WsProvider(endpoint);
    api = await ApiPromise.create({ provider: wsProvider });
    keyring = new Keyring({ type: 'sr25519' });
    newPair = keyring.addFromUri(PHRASE);
  });

  it("Will printout data",function(){
    let ss58a=newPair.address
    let hexa=u8aToHex(newPair.publicKey)
    console.log(ss58a)
    console.log(hexa)
    assert(!isHex(ss58a))
    assert(isHex(hexa))
  });

  it("Will convert from SS58 to Public key/Account ID",function(){
    let ss58a=newPair.address
    let hexa=u8aToHex(newPair.publicKey)
    let hexa2=u8aToHex(decodeAddress(ss58a));
    assert(hexa===hexa2);
  });

  it("Will convert from Account ID to SS58",function(){
    let ss58a=newPair.address
    let hexaFrom=u8aToHex(newPair.publicKey)
    console.log(hexaFrom);
    let ss582=encodeAddress(hexaFrom);
    assert(ss58a===ss582);
  });

  it("Will convert from Account ID to SS58 to Public key via keyring",function(){
    // The test Address
    let ss58a=newPair.address
    // ITs Hex Account ID.
    let hexa=u8aToHex(newPair.publicKey);
    let kr=new Keyring({ type: 'sr25519' });
    let kp=kr.addFromAddress(hexa);
    assert(kp.address === ss58a);
  });

  it("Will convert one account id to ss58", function(){
    const accid="0xfff437ff18629bf1490e5c9b3ec6f1515d46bb9b2aeaa6e39e36611f2479b50d";

    // Convert address using a KeyRing
    let kr=new Keyring({ type: 'sr25519' });
    let kp=kr.addFromAddress(accid);
    kr.setSS58Format(0); // Polkadot
    console.log(kp.address);
    // Test Alternative Way to Encode, without the Keyring
    let acc=hexToU8a(accid);
    console.log(acc);
    let ss58b=encodeAddress(acc,0); // Polkadot
    console.log(ss58b);
    assert(kp.address===ss58b);
  });

  it("Will get a key owner", async function () {
    const raw = await api.query.session.keyOwner(['0x696d6f6e','0xa6ab108e0d10b8248469d38dd81c1dac29ccb53a9281577057077a062095b642']);
    console.log(raw.toHex());
    let kr=new Keyring({ type: 'sr25519' });
    let kp=kr.addFromAddress(raw.toHex());
    kr.setSS58Format(0); // Polkadot
    assert(kp.address === "15mURTf3t8dEvEY6Gb5N76wm9t2jQcXzewXmjT3yfZA1w8si");
    console.log(kp.address);
  });

  it("Will get a key owner with our utility", async function(){
    const owner=await substrateKeyOwner(api,"0xa6ab108e0d10b8248469d38dd81c1dac29ccb53a9281577057077a062095b642");
    console.log(owner);
    assert(owner === "15mURTf3t8dEvEY6Gb5N76wm9t2jQcXzewXmjT3yfZA1w8si");
  });

  it("Will get another unknown key owner with our utility", async function(){
    const owner=await substrateKeyOwner(api,"0x6a5e7fcc5576c262a7f09146b055d7e544e552a174beaf10cccdff35043daa22");
    console.log(owner);
    assert(owner === "UNKNOWN");
  });

  it("Will get a key owner of an old block", async function () {
    let owner="14mXmQkh5A2o2v6UpYX5Jv33pNb4KEAYhrEsMWsPg2L8DGES";
    owner=u8aToHex(decodeAddress(owner));
    console.log(owner);
    const raw = await api.query.session.keyOwner(['0x696d6f6e',owner]);
    console.log(raw);
    let kr=new Keyring({ type: 'sr25519' });
    let kp=kr.addFromAddress(raw.toHex());
    kr.setSS58Format(0); // Polkadot
    console.log(kp.address);
    assert(kp.address === "15mURTf3t8dEvEY6Gb5N76wm9t2jQcXzewXmjT3yfZA1w8si");
  });

  it("Will get a key owner of an old block with our utility", async function(){
    const owner=await substrateKeyOwner(api,"14mXmQkh5A2o2v6UpYX5Jv33pNb4KEAYhrEsMWsPg2L8DGES")
    console.log(owner);
    assert(owner === "15mURTf3t8dEvEY6Gb5N76wm9t2jQcXzewXmjT3yfZA1w8si");
  });


  it("Will detect the identity of a validator with hex display name", async function(){
    const v0="1MrurrNb4VTrRJUXT6fGxHFdmwwscqHZUFkMistMsP8k5Nk";
    let info = await api.query.identity.identityOf(v0);
    info = info.unwrap().toHuman();
    console.log(info);
    let displayName=decodeInfoField(info.info.display);
    console.log(displayName);
    assert(displayName.indexOf("DWELLIR")>0);
  });

  it("Will get parent info of validator with hex display and no parent", async function(){
    const v0="1MrurrNb4VTrRJUXT6fGxHFdmwwscqHZUFkMistMsP8k5Nk";
    const parentInfo=await getParentInfo(api,v0);
    assert(parentInfo.name.indexOf("DWELLIR")>0);
  });

  it("Will get parent info of validator with hex display and parent", async function(){
    const v0="1MrurrNb4VTrRJUXT6fGxHFdmwwscqHZUFkMistMsP8k5Nk";
    const parentInfo=await getParentInfo(api,v0);
    assert(parentInfo.name.indexOf("DWELLIR")>0);
  });


  after("Will close the API", function(){
    api.disconnect();
  });

});