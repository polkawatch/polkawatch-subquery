import {describe, it} from "mocha";

import chai from "chai";
const {assert}=chai;

import VM from "@subql/x-vm2";

import jsEvents from "jsEvents";


describe("It will test using stuff inside the sandbox",function(){

    it("Will test teh console in Sandbox", function(){

        class TestVM extends VM.NodeVM{
            constructor(args={}){
                super(args)
            }
        }

        let vm=new TestVM();
        vm.run("console.log('hello world');");
    });


    it("It will test the events builtin mocking", function(){
        const vm= new VM.NodeVM({ require: { builtin: ['events'], mock:{events:jsEvents} }});
        vm.run(`require('events')`);
    });

});