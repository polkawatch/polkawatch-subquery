/**
 * This is an array of arrays,
 * each array is a sample of a group.
 * each member if the array is a member of the group
 */
export const ValidatorGroupSamples = [
  // Samples from Pos.dog
  {
    parent: "",
    validators: [
      "1LMtHkfrADk7awSEFC45nyDKWxPu9cK796vtrf7Fu3NZQmB",
      "15QbBVsKoTnshpY7tvntziYYSTD2FyUR15xPiMdpkpJDUygh",
      "16A1zLQ3KjMnxch1NAU44hoijFK3fHUjqb11bVgcHCfoj9z3"
    ]

  },
  // Samples from Zug Capital
  {
    parent: "",
    validators: [
      "1zugcaQMPJ1s5Snjy51hTReuoth4YLd39VmuRjTsxArFfmq",
      "1zugcacYFxX3HveFpJVUShjfb3KyaomfVqMTFoxYuUWCdD8",
      "1zugcaebzKgKLebGSQvtxpmPGCZLFoEVu6AfqwD7W5ZKQZt"
    ]

  },

  // Samples from Binance
  // This is a complex sample, they seem to have a programmatic, dynamic setup
  {
    parent: "",
    validators: [
      "114SUbKCXjmb9czpWTtS3JANSmNRwVa4mmsMrWYpRG1kDH5",
      "12771k5UXewvK7FXd1RpPHxvFiCG4GQCrxRmXWN5tAAwDQoi",
      "15kb76J3G8QvtoYKZdWyvPC12MzewsMYYneYT7E6SM2R3oTB"
    ]

  }
  ,

]
