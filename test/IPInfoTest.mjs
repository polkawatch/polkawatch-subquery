import {describe, it} from "mocha";
import chai from "chai";
import "it-fails";

const {assert}=chai;


import geolite2 from "geolite2-redist";
import maxmind from "maxmind";
import Promise from "bluebird";
import dns from "dns";

let dnsLookupAsync = Promise.promisify(dns.lookup);


describe("Will test ASN lookup", function(){
  let ASNLookup,
    ContryLookup,
    testIp;

  before("Will download Geolite Databases", async function(){
    this.timeout(600*1000);
    await geolite2.downloadDbs();
    ASNLookup = await geolite2.open('GeoLite2-ASN', path => maxmind.open(path));
    ContryLookup = await geolite2.open('GeoLite2-Country', path => maxmind.open(path));
    testIp = await dnsLookupAsync("polkawatch.app");
  });

  it("Will get the ASN of a well known IP4", async function(){
    let testASN = ASNLookup.get(testIp);
    assert(testASN.autonomous_system_number === 8473 );
    console.debug(testASN);
  });

  it("Will get the City of a well known IP4", function() {
    let testCountry = ContryLookup.get(testIp);
    assert(testCountry.country.iso_code === 'SE' );
    console.debug(testCountry);
  });

  after("WIll close the Geolite DB", function(){
    ASNLookup.close();
    ContryLookup.close();
  });

});