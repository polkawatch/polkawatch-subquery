import {describe, it} from "mocha";
import "it-fails";

import chai from "chai";
const {assert}=chai;

import { ApiPromise, WsProvider } from '@polkadot/api';
import {getEraStaking, getPayoutEra, recordPayoutEra} from "../src/utils/SubstrateKey.mjs";

const endpoint= process.env.PW_POLKADOT_RPC || 'wss://rpc.polkadot.io';


describe("It will test rewards utlilities", function(){
    this.timeout(100000);
    let api;

    before("Will Create the API ", async function(){
        const wsProvider = new WsProvider(endpoint);
        api = await ApiPromise.create({ provider: wsProvider });
    });

    it("WIll get the stakers for an era", async function(){
        let era=510;
        let r= await getEraStaking(api,era);
        console.log(`there are ${Object.keys(r).length} stakers in era ${era}`);
        // this one should come form cache
        r= await getEraStaking(api,era);
    });

    after("Will close the API", function(){
        api.disconnect();
    });

});


describe("Will test event matching", function(){

    it("Will test payout era recording and retrieval", function(){
        // we consider a block with 3 payouts
        recordPayoutEra(999,7, 509);
        recordPayoutEra(999,17, 510);
        recordPayoutEra(999,20, 511);
        recordPayoutEra(1000,5, 509);
        recordPayoutEra(1000,20, 510);
        recordPayoutEra(1000,40,507);

        // now we process reward events that happen at interleaved indexes

        assert(getPayoutEra(1000,1)===undefined);
        assert(getPayoutEra(1000,7)===509);
        assert(getPayoutEra(1000,47)===507);
        assert(getPayoutEra(1000,39)===510);


    });


});