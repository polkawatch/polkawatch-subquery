import {describe, it} from "mocha";
import chai from "chai";
import "it-fails";

const {assert}=chai;

import { ApiPromise, WsProvider } from '@polkadot/api';

import {ValidatorGroupSamples} from "./ValidatorData.mjs";

// Allow for endpoints to be configured later on
const endpoint= process.env.PW_POLKADOT_RPC || 'wss://rpc.polkadot.io';

describe("Polkadot Chain Interaction",function(){
  this.timeout(10000);
  let api;

  before("Will Create the API ", async function(){

    const wsProvider = new WsProvider(endpoint);
    api = await ApiPromise.create({ provider: wsProvider });
  });

  it("Will check information", async function(){
    // get the chain information
    const chainInfo = await api.registry.getChainProperties();
    assert(chainInfo.has('ss58Format'));
    console.log(chainInfo);
  });


  it("Will get the parent account of a known validator", async function(){
    this.timeout(1000000);
    // pick a validator group
    const vg = ValidatorGroupSamples[0];
    // pick 2 in the group
    const v0 = vg.validators[0];
    const v1 = vg.validators[1];
    async function getParent(validator){
      const raw = await api.query.identity.superOf(validator);
      const parent = raw.unwrap().toHuman()[0];
      return parent;
    }

    // assert( p1 === p2); --> this will pass with undefined
    //simple way to test
    // const wkParent = '13KJ3t8w1CKMkXCmZ6s3VwdWo4h747kXE88ZNh6rCBTvojmM'
    // this.timeout(100000);
    // assert(await getParent(v0) === wkParent && await getParent(v1) === wkParent);

    // more generic:
    const parent1 = await getParent(v0);
    const parent2 = await getParent(v1);
    const t1 = ((typeof parent1 === 'string' || parent1 instanceof String) && parent1.length === 48);
    assert((parent1 === parent2) && t1);
  });

  it("Will get the identity of a validator", async function(){
    const vg = ValidatorGroupSamples[0];
    // pick 2 in the group
    const v0 = vg.validators[0];
    let parent = await api.query.identity.superOf(v0);
    parent=parent.unwrap().toHuman()[0];
    console.log(parent);
    let info = await api.query.identity.identityOf(parent);
    info = info.unwrap().toHuman();
    console.log(info);

    assert(info.info.display !== "None");
    assert(info.info.display.Raw === "pos.dog");
    assert(info.info.web.Raw === "https://pos.dog");
    assert(info.info.legal === 'None');
  });

  it("Will detect that the parent has no parent", async function(){
    const v0 = "13KJ3t8w1CKMkXCmZ6s3VwdWo4h747kXE88ZNh6rCBTvojmM";
    let parent = await api.query.identity.superOf(v0);
    assert(parent.isNone);
  });

  it("Will detect that the validator has no info", async function(){
    const vg = ValidatorGroupSamples[0];
    const v0 = vg.validators[0];
    let info = await api.query.identity.identityOf(v0);
    assert(info.isNone);
  });

  it("Will test a multi query", async function(){
    const vg = ValidatorGroupSamples[0];

    // Subscribe to balance changes for 2 accounts, ADDR1 & ADDR2 (already defined)
    const unsub = await api.query.system.account.multi(vg.validators, (balances) => {
      const [{ data: balance1 }, { data: balance2 }] = balances;
      console.log(`The balances are ${balance1.free} and ${balance2.free}`);
    });

  });

  it("Will test a multi query with multiple arguments", async function(){
    let era=510;
    const vg = ValidatorGroupSamples[0];
    let args = vg.validators.map((v) => [era,v]);
    console.log(args);
    const unsub = await api.query.staking.erasStakers.multi(args, (stakers) => {
      stakers.map((s,i) => {
        s=s.toHuman();
        console.log(`result for ${args[i][1]} is:`);
        console.log(s);
      });
      console.log(stakers);
    });

  });

  it("will learning what the active validator is for an account", async function(){
    const era=510;
    // Era Rewards points also tells us active validaors
    const eraRewardPoints = await api.query.staking.erasRewardPoints(era);
    const eraValidators=Object.keys(eraRewardPoints.toHuman().individual);
    console.log(eraValidators);

    // Nominators exposed to validator in a given ERA
    let r=await api.query.staking.erasStakers(era,"1MrurrNb4VTrRJUXT6fGxHFdmwwscqHZUFkMistMsP8k5Nk");
    console.log(r.toHuman());

  });


  it("Will calculate validator payout in float", async function(){
    this.timeout(3000000);
    const current_era = await api.query.staking.currentEra();
    // console.log('test ', current_era.unwrap().words[0]);
    const era_to_test = (parseInt(current_era.unwrap().words[0]) - 1).toString();
    const eraRewardPoints = await api.query.staking.erasRewardPoints(era_to_test);
    // console.log('test2 ', eraRewardPoints.toHuman());
    const totalRewardPoints = parseFloat(eraRewardPoints.toHuman().total.replace(/(?!\/)(?!\ )(?!\-)(\W)/ig, ''));
    let rewardPointsSum = 0;
    let validatorShares = {};
    let vsSum = 0;
    for (let [key, value] of Object.entries(eraRewardPoints.toHuman().individual)) {
      rewardPointsSum += parseInt(value.replace(/(?!\/)(?!\ )(?!\-)(\W)/ig, ''));
      validatorShares[key] = (100.0/totalRewardPoints)*parseFloat(value.replace(/(?!\/)(?!\ )(?!\-)(\W)/ig, ''));
      vsSum += (100.0/totalRewardPoints)*parseFloat(value.replace(/(?!\/)(?!\ )(?!\-)(\W)/ig, ''));
    }
    // console.log(rewardPointsSum)
    // console.log('test 3', eraRewardPoints.toHuman().total.replace(/(?!\/)(?!\ )(?!\-)(\W)/ig, ''));

    // console.log(validatorShares);
    // console.log('100%', vsSum);


    assert(rewardPointsSum === parseInt(eraRewardPoints.toHuman().total.replace(/(?!\/)(?!\ )(?!\-)(\W)/ig, '')));
    assert(vsSum > 99.9999);

    
    const totalRewardsRaw = await api.query.staking.erasValidatorReward(era_to_test);
    const totalRewards = parseFloat(totalRewardsRaw.unwrap().toHuman().replace(/(?!\/)(?!\ )(?!\-)(\W)/ig, ''))

    // console.log(totalRewards);

    let validatorRewardsAbsolute = {};
    let pcSum = 0;
    for (let [key, value] of Object.entries(validatorShares)) {
      const multiplier = value*0.01;
      // console.log(totalRewards * multiplier);
      pcSum += totalRewards * multiplier;
      validatorRewardsAbsolute[key] = totalRewards * multiplier
    }

    // console.log(validatorRewardsAbsolute);
    // console.log(pcSum);
    // console.log(totalRewards);

    for (let [key, value] of Object.entries(validatorRewardsAbsolute)) {
      const valPrefsRaw = await api.query.staking.erasValidatorPrefs(era_to_test,key);
      // console.log(parseFloat(valPrefsRaw.toHuman().commission));
      const commission = parseFloat(valPrefsRaw.toHuman().commission)
      const multiplier = commission*0.01;
      // console.log(typeof value)
      const validatorAbsoluteProfit = (value*0.5) + (value*0.5*multiplier);
      console.log(`validator total reward ${value} \nvalidator comission ${commission} % \nvalidator share of total reward ${validatorAbsoluteProfit} \nnominator share of total reward ${value - validatorAbsoluteProfit}\n\n`)
    }

  });  

  after("Will close the API", function(){
    api.disconnect();
  });

});
