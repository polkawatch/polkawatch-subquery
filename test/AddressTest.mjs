import {describe, it} from "mocha";
import "it-fails";


import chai from "chai";
const {assert}=chai;

import  { Multiaddr } from "multiaddr";
import is_private_ip from "private-ip";

import {purgePrivateMultiaddresses,purgePrivateMultiaddressesRE, getPublicIP4RE, multiaddressRegexp, getNetworkGroupName } from "../src/utils/Multiaddress.mjs";

describe("Will test IP Address",function(){

  it("Will test loooback", function(){
    const addr =  new Multiaddr("/ip4/127.0.0.1/udp/1234");
    assert(addr.nodeAddress().family === 4);
    assert(addr.nodeAddress().address === "127.0.0.1");
  });

  it("Will check capability to check for private ip v4", function(){
    [
      "127.0.0.1",
      "192.168.0.1",
      "10.0.0.1",
      "172.16.0.1"
    ].map((ip)=>{
      const addr =  new Multiaddr(`/ip4/${ip}/udp/1234`);
      assert(is_private_ip(addr.nodeAddress().address));
    });

    [
      "8.8.8.8",
      "1.1.1.1"
    ].map((ip)=>{
      const addr =  new Multiaddr(`/ip4/${ip}/udp/1234`);
      assert(!is_private_ip(addr.nodeAddress().address));
    })

  });

  it.fails("Will test purging private addresses form a multiaddress",function(){
    const addresses=[
      "/ip4/127.0.0.1/udp/1234",
      "/ip4/192.168.0.1/udp/1234",
      "/ip4/1.1.1.1/udp/1234",
      "/ip4/10.0.0.1/udp/1234",
      "/ip4/172.16.0.1/udp/1234"
    ];

    let result=purgePrivateMultiaddresses(addresses);
    assert(result.length === 1)
  });

  it.fails("Will test purging private addresses form a multiaddress with several public ips",function(){
    const addresses=[
      "/ip4/127.0.0.1/udp/1234",
      "/ip4/192.168.0.1/udp/1234",
      "/ip4/1.1.1.1/udp/1234",
      "/ip4/10.0.0.1/udp/1234",
      "/ip4/172.16.0.1/udp/1234",
      "/ip4/8.8.8.8/udp/1234"
    ];

    let result=purgePrivateMultiaddresses(addresses);
    assert(result.length === 2)
  });


  it.fails("Will test purging private addresses form a multiaddress with an invalid entry",function(){
    const addresses=[
      "invalid entry",
      "/ip4/192.168.0.1/udp/1234",
      "/ip4/1.1.1.1/udp/1234",
      "/ip4/10.0.0.1/udp/1234",
      "/ip4/172.16.0.1/udp/1234",
      "/ip4/8.8.8.8/udp/1234"
    ];

    let result=purgePrivateMultiaddresses(addresses);
    assert(result.length === 2)
  });

  // WIP: issue #1 Multiaddr will not work inside the VM
  it("Will try to parse a multiaddress with a regexp", function(){
    const address="/ip4/127.0.0.1/udp/1234";
    assert(multiaddressRegexp.test(address));
    let ret=multiaddressRegexp.exec(address);
    assert(ret[1]==="4");
    assert(ret[2]==="127.0.0.1");
    assert(ret[3]==="udp");
    assert(ret[4]==="1234");
  });

  it("Will test purging private addresses form a multiaddress using RegExp implementation",function(){
    const addresses=[
        "/ip4/127.0.0.1/udp/1234",
        "/ip4/192.168.0.1/udp/1234",
        "/ip4/1.1.1.1/udp/1234",
        "/ip4/10.0.0.1/udp/1234",
        "/ip4/172.16.0.1/udp/1234"
    ];

    let result=purgePrivateMultiaddressesRE(addresses);
    assert(result.length === 1)
  });

  it("Will test purging private addresses form a multiaddress with several public ips using RegExp implementation",function(){
    const addresses=[
        "/ip4/127.0.0.1/udp/1234",
        "/ip4/192.168.0.1/udp/1234",
        "/ip4/1.1.1.1/udp/1234",
        "/ip4/10.0.0.1/udp/1234",
        "/ip4/172.16.0.1/udp/1234",
        "/ip4/8.8.8.8/udp/1234"
    ];

    let result=purgePrivateMultiaddressesRE(addresses);
    assert(result.length === 2)
  });


  it("Will test purging private addresses form a multiaddress with an invalid entry using RegExp implementation",function(){
    const addresses=[
        "invalid entry",
        "/ip4/192.168.0.1/udp/1234",
        "/ip4/1.1.1.1/udp/1234",
        "/ip4/10.0.0.1/udp/1234",
        "/ip4/172.16.0.1/udp/1234",
        "/ip4/8.8.8.8/udp/1234"
    ];

    let result=purgePrivateMultiaddressesRE(addresses);
    assert(result.length === 2)
  });

  // TODO: No idea what that first character is, can't see any reference in the spec.
  it("Will test purging private addresses form a multiaddress with prefix character using RegExp implementation",function(){
    const addresses=  ["d/ip4/35.198.36.168/tcp/80","p/ip4/35.198.36.168/tcp/30333","d/ip4/172.18.0.2/tcp/30333","`/ip4/127.0.0.1/tcp/30333","p/ip4/192.168.19.64/tcp/30333","l/ip4/192.168.29.0/tcp/30333"];
    let result=purgePrivateMultiaddressesRE(addresses);
    assert(result.length === 2)
  });


  it("Will extract public ip4 ammong duplicates, invalid entries and private ips",function(){
    const addresses=  ["d/ip4/35.198.36.168/tcp/80","p/ip4/35.198.36.168/tcp/30333","invalid","d/ip4/172.18.0.2/tcp/30333","`/ip4/127.0.0.1/tcp/30333","p/ip4/192.168.19.64/tcp/30333","l/ip4/192.168.29.0/tcp/30333"];
    let result=getPublicIP4RE(addresses);
    assert(result.length === 1)
    assert(result[0]==="35.198.36.168");
  });

  it("Will test ASN group names", function(){

    assert(getNetworkGroupName("GOOGLE") === "GOOGLE");
    assert(getNetworkGroupName("GOOGLE-PRIVATE-CLOUD") === "GOOGLE");
    assert(getNetworkGroupName("OVH SAS") === "OVH");
    assert(getNetworkGroupName("DIGITALOCEAN-ASN") === "DIGITALOCEAN");
    assert(getNetworkGroupName("Hetzner Online GmbH") === "HETZNER");
    assert(getNetworkGroupName("Alibaba US Technology Co., Ltd.") === "ALIBABA");
    assert(getNetworkGroupName("AMAZON-AES") === "AMAZON");

    assert(getNetworkGroupName("IONOS SE") ==="IONOS SE");
    assert(getNetworkGroupName("Contabo GmbH") ==="Contabo GmbH");
    assert(getNetworkGroupName("23M GmbH") ==="23M GmbH");

  });

});