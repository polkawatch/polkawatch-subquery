import {describe, it} from "mocha";
import "it-fails";

import chai from "chai";
const {assert, expect}=chai;

import {Validator} from "../src/utils/SubstrateAccounts.mjs";

describe("Will test extracting features from validators",function(){

  // TODO: we need a well known validator for testing
  const VID="well-known-validator-id";
  const VIP="well-known-validator-ip";
  const VASN="well-known-validator-asn";
  const V = new Validator(VID);

  it.fails("Will test the IP Address of a well known validator", function(){
    assert(V.getMultiaddrs().nodeAddress().address === VIP);
  })

  it.fails("Will test the ASN of a well known validator", function(){
    assert(V.getASN() === VASN);
  })

});