import {describe, it} from "mocha";
import "it-fails";

import chai from "chai";
const {assert, expect}=chai;

import {Client} from "@elastic/elasticsearch";

const client = new Client({ node: 'http://localhost:9200' });


describe("It will test elastic ", function(){

    //TODO: move the container init, required for setup, fails if already there
    it.fails("Will create a pw_payout index", async function(){
        let r = await client.indices.create({
            index: "pw_payout",
            body: {
                mappings: {
                    properties: {
                        date: {type: "date"},
                        era: {type: "long"},
                        validator: {type: "keyword"},
                        validator_parent: {type: "keyword"},
                        validator_parent_name: {type: "keyword"},
                        validator_parent_web: {type: "keyword"},
                        validator_parent_legal: {type: "keyword"},
                        validator_country_group_code: {type: "keyword"},
                        validator_country_group_name: {type: "keyword"},
                        validator_country_code: {type: "keyword"},
                        validator_country_name: {type: "keyword"},
                        validator_asn_code: {type: "keyword"},
                        validator_asn_name: {type: "keyword"},
                        validator_asn_group: {type: "keyword"}
                    }
                }
            }
        });

    });

    //TODO: move the container init, required for setup, fails if already there
    it.fails("Will create a pw_reward index", async function(){
        let r = await client.indices.create({
            index: "pw_reward",
            body: {
                mappings: {
                    properties: {

                        date: {type: "date"},
                        era: {type: "long"},
                        nominator:  {type: "keyword"},

                        reward: {type: "long"},
                        nomination_value: {type: "long"},

                        validator: {type: "keyword"},
                        validator_parent: {type: "keyword"},
                        validator_parent_name: {type: "keyword"},
                        validator_parent_web: {type: "keyword"},
                        validator_parent_legal: {type: "keyword"},


                        validator_country_group_code: {type: "keyword"},
                        validator_country_group_name: {type: "keyword"},
                        validator_country_code: {type: "keyword"},
                        validator_country_name: {type: "keyword"},
                        validator_asn_code: {type: "keyword"},
                        validator_asn_name: {type: "keyword"},
                        validator_asn_group: {type: "keyword"}
                    }
                }
            }
        });
    });

});